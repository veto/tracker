package com.myridia.android.tracker;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Manager;
import com.couchbase.lite.android.AndroidContext;
import com.couchbase.lite.util.ZipUtils;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
        final String TAG = "tracker";
        String dbname = "foodb";
        String dburl = "http://bar.com:5984/foodb";
        Manager manager = null;
        Database database = null;
        try {
          manager = new Manager(new AndroidContext(this), Manager.DEFAULT_OPTIONS);
            database = manager.getDatabase(dbname);
            Log.e(TAG, "hello");

        } catch (Exception e) {
            Log.e(TAG, "Error getting database", e);
            return;
        }


  }
}
